NumPy: Uses array-based calculations with nested lists
Example:
   In [1]: matrix = [[1, 2, 3],
                      [4, 5, 6],
                      [7, 8, 9]]
In[2]:[[i+1foriinrow]forrowinmatrix]
 Out[2]: [[2, 3, 4], [5, 6, 7], [8, 9, 10]]

One Dimensional Array:
-Only has one axis
-No explicit column or row orientation
-Always has a column or row orientation
Example:
In [6]: array1.dtype
Out[6]: dtype('float64')
Broadcasting: NumPy extends smaller rays across the larger arrat so their shapes become compatible
Universal Fundtion (ufunc): Works on every element in a NumPy array
NumPy Array needs to be of the same data type
